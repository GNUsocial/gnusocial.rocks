<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=5.0, minimum-scale=0.86">
  <title>GNU social &mdash; Some public communities</title>
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="stylesheet" href="https://www.gnusocial.rocks/assets/css/style.css">
  <link rel="stylesheet" href="https://www.gnusocial.rocks/assets/css/reset.css">
<?php
    $query = urlencode('
    {
      nodes(platform: "gnusocial") {
        openSignups
        name
        host
        countryCode
      }
    }
    ');
    $query_result = json_decode(file_get_contents("https://the-federation.info/graphql?query={$query}"), true);
    $query_result = $query_result['data']['nodes'];
    // Filter out instances with closed signups
    $nodes = array_filter($query_result, function ($node) {
        return $node['openSignups'];
    });
    // garbage collect
    unset($query_result);
?>
</head>

<body>
  <header class="header">
      <div class="home-menu menu menu-horizontal  menu-fixed">
          <a class="menu-heading" href="https://gnusocial.rocks/">
              <img src="https://www.gnusocial.rocks/assets/img/gnu-social-logo-invert.svg" alt="GNU social">
          </a>
        <ul class="menu-list">
          <li class="menu-item"><a href="/" class=" menu-link">Home</a></li>
          <li class="menu-item"><a href="/v2" class=" menu-link">Version 2</a></li>
          <li class="menu-item"><a href="/v3" class=" menu-link">Version 3</a></li>
        </ul>
      </div>
  </header>
  <div class="splash-container">
    <div class="splash">

      <h2 class="content-head is-center">Some public GNU social communities</h2>
        <p>Please note that the servers listed here are not run by us and we are not responsible for their operation or
        their content. They are listed here as a service to the community.</p>

        <h3>Public GNU social Servers</h3>
        <table id="nodes">
        <thead>
                <tr>
                <th>Name</th>
                <th>Country</th>
                </tr>
        </thead>
        <tbody>
        <?php foreach ($nodes as $node): ?>
                <tr>
                <td>
                        <a href="https://<?php echo $node['host']; ?>"><?php echo $node['name']; ?></a>
                </td>
                <td>
                        <?php echo $node['countryCode']; ?>
                </td>
                </tr>
        <?php endforeach; ?>
        </tbody>
        </table>
        <p><br>This table's data comes from <a href="https://the-federation.info/">the federation - a statistics hub</a>, if
        you want to be listed here, please go to https://the-federation.info/register/&#x3C;yournode.tld&#x3E;.
        After some seconds, you should see your node added.</p>
    </div>

  </div>

  <footer class="l-box is-center">
      This site's source is <a href="https://code.undefinedhackers.net/GNUsocial/gnusocial.rocks">hosted here</a>.
  </footer>
</body>

</html>
