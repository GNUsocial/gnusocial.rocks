Updates: Finish the Avatar component

[Avatar Component source](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Avatar).

Its controller handles upload, update and removal.

Important change from v2: Avatars are now regular attachments.
