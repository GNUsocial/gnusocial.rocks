Milestone: Documentation and Tests Infrastructure

**[>WIKI Milestone entry](https://agile.gnusocial.rocks/doku.php?id=milestones:initial_test_documentation_infrastructure)**

GNU social now has its documentation available in
[https://docs.gnusocial.rocks/](https://docs.gnusocial.rocks/). It features four
different books. These are automatically generated from the [source](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/docs) using [mdBook](https://rust-lang.github.io/mdBook/).

> Only the development book is in an elaborated state, the other books are
> holding for more ready code.

And two of them are new:

* The [Developer](https://docs.gnusocial.rocks/developer) is both intended to guide third-party plugin developers and to make it easier of contributing to the code.
* The [Designer](https://docs.gnusocial.rocks/designer) is the most recent of the four and came from a necessity of keeping some standardization between templates and ensuring the same principles are kept in mind when designing new themes.

And two of them are updates from existing documentation:

* The [User](https://docs.gnusocial.rocks/user) one is adapted
from the existing GNU social documentation for users that was provided in v2.
* The [Administrator](https://docs.gnusocial.rocks/administrator) one is adapted
from the "Unofficial GNU social docs" by Thomask who [asked us to make it official](https://notabug.org/diogo/gnu-social/issues/246).

Together with the documentation we've introduced a
[wiki](https://agile.gnusocial.rocks/). Its purpose is to walk-through decisions,
convention, terminology. It's where we document the reasoning the development team went
through before implementing more sophisticated functionalities.

Finally, when the documentation doesn't explain, and to ensure the whole code
is properly tested, we have the
[tests](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/tests). And the coverage is available [here](https://coverage.gnusocial.rocks/). At the time of writing the coverage has 98.76% code lines tested.
