Milestone: Webhooks

GNU social has [always been a project dear to hackers](https://web.archive.org/web/20190320033635/https://chromic.org/blog/lifestream-architecture/).

And it's because we love the project history and nature of being easily
extensible and capable of communicating with the most diverse platforms that we
insist on implementing robust and expressive specifications.

Want the notes mentioning you showing up on your toasts, then just code a quick
script and GNU social will definitely not be the one holding you back. We can
think of this [old GNU social <-> Arduino bridge](https://notabug.org/HackersAtPorto/gs-arduino).

Webhooks is thus one more natural step on this direction.

![](assets/webhooks/settings.png)
![](assets/webhooks/example.png)
