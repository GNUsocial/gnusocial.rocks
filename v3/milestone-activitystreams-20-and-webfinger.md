Milestone: ActivityStreams 2.0 and WebFinger

The primary use of GNU social is to access the [free network](https://blog.diogo.site/what-is-the-fediverse), be it ActivityWeb (ActivityPub) or Fediverse (OStatus).

Contrary to the original plan, we have merged [The Free Network Module](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/nightly/modules/TheFreeNetwork), [WebFinger](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/nightly/plugins/WebFinger) and [LRDD](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/nightly/plugins/LRDD) into a single component named [FreeNetwork](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/FreeNetwork). Likewise, ActivityPub and ActivityStreams 2.0 was kept the same plugin instead of separated.

## Understanding the organisation chosen

The FreeNetwork component adds WebFinger (RFC7033) lookup and implements Link-based Resource Descriptor Discovery (LRDD) based on RFC6415, Web Host Metadata. It takes and produces both Extensible Resource Descriptor (XRD) and JSON (JavaSript Object Notation). Furthermore, and different from v2, every federation protocol will use the same distribution queue maintained by this component instead of holding its own.

We originally intended to have data modelling plugins that would extend the GS's "language". We then understood that it added more complexity than we wanted without any considerable advantage because we cannot dissociate data reception handling of the protocol itself.

## Situation Report

[ActivityPub](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub) already translates between activity and entity and allows plugins to extend it (thus serving a similar purpose to data modelling and representation plugins).

GNU social v3 now supports [mentions](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/src/Util/Formatting.php#L292), which is a process that starts in the [Posting component](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Posting/Posting.php#L197). The processing of local mentions naturally finds its entire handling here.

For remote ActivityPub mentions, [ActivityPub handles it aided by the FreeNetwork component]((https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/Entity/ActivitypubActor.php#L179)).

## Next steps

We still have to port OStatus (and ActivityStreams 1.0) and implement the distribution by FreeNetwork, although the base work is done. Regarding ActivityPub, although some of it already works, expanding the existing plugins to supplement ActivityPub, and full validation isn't ready yet. We will most likely finish the implementation of the whole federation stack in the next week.

