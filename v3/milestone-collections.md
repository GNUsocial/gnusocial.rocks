Milestone: Collections

We have generalized collections around our search engine. For that introducing
the [Collection component](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Collection).

This structure has the following implication:

    Collection -> OrderedCollection
    |-> Notes (known as Feed)
    |-> Actors (known as Circle)
    |-> Other Object (known as Collection of ...)
      |-> Attachments: AttachmentCollection
      |-> Tags: TagCollection

And it allow us to centralise the scope system in the core, ensuring that no
actor will have access to notes or other objects that shouldn't be able to see.

Finally, with this, a plugin such as [Note Type Feed Filter](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/NoteTypeFeedFilter) can be useful in
every note collection (feed).

It also allows plugins such as ActivityPub to [translate every collection
automatically](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/Util/OrderedCollectionController.php).

We also introduced the [Circle component](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Circle), which has a UI similar to the
[Attachments Collections](https://www.gnusocial.rocks/v3/milestone-albums.html),
and enables circle mentions with `@#circle_name`.

## Notes Filter and Sorting Options
![](assets/filters/feed.png)

## Actors Filter and Sorting Options
![](assets/filters/circle.png)

## The Search Component

### Results
![](assets/search/results.png)

### Note Options
![](assets/search/note_options.png)

### Actor Options
![](assets/search/actor_options.png)

## ActivityPub Examples
![](assets/activitypub/collection/circle.jpg)
![](assets/activitypub/collection/favourites.jpg)
![](assets/activitypub/collection/outbox.jpg)

