Milestone: Pinned Notes
You can now pin notes in your profile! Given you have suficient permissions to do so (e.g. you created the note), an additional action is represented, allowing you to highlight your favourite notes.

With this feature, an actor has more ways to express itself to the community as a whole. This activity is federated using [Mastodon's featured collection extension](https://docs.joinmastodon.org/spec/activitypub/#featured).

[Source](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/PinnedNotes)
![Default view of the actor profile, showing the 'Pin this note' action, and its respective representation](assets/profile/pin_note.png)

