Milestone: Mute notifications from a conversation

If a conversation in which you have interacted becomes very active and you wish
to stop receiving notifications derived from that, it is now possible:
![](assets/conversation/mute.png)
