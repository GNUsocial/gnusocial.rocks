Updates: Local Groups

![](assets/groups/profile.png)

We haven't implemented federation of Groups yet, but group tags and [discussion
on how unbounded groups can be federated via ActivityPub](https://socialhub.activitypub.rocks/t/decentralised-group/2200/17?u=diogo) has already started.

Concerning federation of traditional groups, we will port our logic from v2 and
translate following the same rules as AndStatus [because they work](https://github.com/andstatus/andstatus/issues/248#issuecomment-558703558), as yvolk beautifully stated :)

Finally, also note that the group actors now have "self-tags", as the other actors, which was a milestone.

