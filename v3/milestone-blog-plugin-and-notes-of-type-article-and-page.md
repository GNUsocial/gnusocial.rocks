Milestone: Blog plugin and Notes of type Article and Page

We have implemented group federation, and [ensured compatibility with Lemmy (MR #2100)](https://github.com/LemmyNet/lemmy/pull/2100/).

This was achieved by strategically introducing the [Blog Plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/Blog).

![](assets/blog/create_article.png)

Related discussion concerning compatibility with Friendica:
[Lemmy issue #2144](https://github.com/LemmyNet/lemmy/issues/2144).

Both GNU social and Lemmy have created test fixtures and the respective tests to
ensure that this now existing compatibility isn't loss. More fixtures are in the
plans.

Various improvements and changes in our Notification component derived from this
development. GNU social and Lemmy have also agreed on expressing and
understanding unlisted Likes in a manner analogous to how we handle Notes. But
GNU social still has to implement support for that form of Like.
