Milestone: OAuth 2.0

OAuth support is accomplished via a GNU social plugin.

Accompanying it came the possibility of a plugin feature a shell script for
setting pre-requirements that can be automatically executed by GNU social's
installer.

There was also an upgrade of the core authentication stack that made it more
simple and flexible.

Finally, the plugin was tested with [AndStatus](http://andstatus.org/), related
discussion available at: [AndStatus #549](https://github.com/andstatus/andstatus/issues/549).
