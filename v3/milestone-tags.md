Milestone: Tags

> Due to the high density of technical aspects, we decided to keep this blog
> post more on the light side and focus on explaining the new functionalities.
> Check our Wiki Milestone entry for all the juicy details.

**[>WIKI Milestone entry](https://agile.gnusocial.rocks/doku.php?id=milestones:tags)**

GNU social v2 has tags and lists. It allows you to:

* search for an `#hashtag` and see a stream of notes tagged with it;
* make lists of actors and mention them with `@#list_name`
* self tag and enter a list of people in your instance with the same self tag

It is limited with regards to federation of self tags and the `@#list_name` can't
target remote actors even when they are inside your list.

## What's new with v3?

### Federated self tags

We now federate self tags and lists, so that constraint from v2 was moved out of
the way.

In the future, the use of these tags can allow you to find people,
groups and even individual notes that have a tag you're interested in. We only
mean filtering, not magic recommendation algorithms.

### Tag Wrangling

Proposed by [@licho](https://archiveofourown.org/users/licho) in Tue, 02 Jun 2019 17:52:07 GMT:

> I like the tag wrangling feature of AO3, which I think would help for cases of synonymous tags like #introduction and #introductions
>
> https://archiveofourown.org/wrangling_guidelines/11
>
> Is it feasible for !gnusocial ? Or would it cause problems?

The answer is **yes** and will be released with v3. With the addition of
[Languages in notes and actors](https://gnusocial.rocks/v3/milestone-notes-and-actors-with-languages.html) there was little excuse not to be feasible.

![](assets/tags/feed-note-tag-run.png)
![](assets/tags/feed-note-tag-running.png)

Whenever you post a note containing tags, you can choose whether to
make those tags canonical. This means that, for instance, the tags
`#run` and `#running` become the 'same', meaning that when you click on
the link for the `#run` tag, you'll also see notes tagged #running. You
can opt out of the behaviour by unchecking the "Make note tags
canonical". An identical process occurs for people tags.

![](assets/tags/checked_make_canonical.png)

Internally, this transformation is accomplished by splitting the tag
into words and [stemming](https://en.wikipedia.org/wiki/Stemming) each word.

### Related Tags

In a tag feed, you can see tags that are often used together with the
one you're seeing. This can be useful, for instance, for finding other
content you'd be interested in.

![](assets/tags/feed-related-notes-shoes.png)
![](assets/tags/feed-related-notes-running.png)

### Improved Tag feeds

![](assets/tags/selftag-feed.png)

When you click on a tag, be it a note tag or a person tag, you'll see
a feed of notes/people with that tag. You can also edit the feeds you
see in your left panel, so you can follow a given tag.

### Mute Self Tags and Note Tags

![](assets/tags/note-options.png)
![](assets/tags/note-muting.png)
![](assets/tags/tag-mute-settings.png)

If you don't like seeing a given tag in your feeds, for whatever
reason, you can choose to mute it. You can mute a note tag or a person
tag, in which case you wouldn't see any notes from people with that
tag.
