Milestone: Notes with Markdown

Markdown content types is now supported.

![](assets/notes/content_type.png)

## Markdown
![](assets/notes/markdown_content.png)
![](assets/notes/markdown_rendered.png)

