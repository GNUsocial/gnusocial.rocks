Milestone: Port Media handling from v2

**[>WIKI Milestone entry](https://agile.gnusocial.rocks/doku.php?id=milestones:attachment_media_embed_handling)**

File Storage in GNU social is used for avatars, for notes containing
attachments, and for notes containing links (in which case is an Embed preview).
Notes can be created by local users or fetched from remote actors. Filehash is
used to reduce file duplication.

When a user shares a Link that uses OpenGraph tags or has an OEmbed provider,
the [Embed plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/Embed) generates a preview for it that may contain a thumbnail.

When a user shares a Link to an image, the [StoreRemoteMedia plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/StoreRemoteMedia) can fetch the
file and make it available as an attachment, and will generate a thumbnail.

When an image, video, or other file type is uploaded or retrieved, an Attachment
entity is created. When a thumbnail is requested, one is generated if an
EncoderPlugin that supports the mime type is available.

There are two EncoderPlugins implemented:

* [ImageEncoder](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ImageEncoder): Allows to compress, validate, rescale, and sanitize images using VIPS.
* [VideoEncoder](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/VideoEncoder): Allows to rescale gifs using FFMpeg.

Another helpful plugin is [FileQuota](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/FileQuota) which ensures a user stays under the file quota.

## How is the code organised?

There are various entities related to attachment and thumbnail handling.
The key ones are:

* [Attachment](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/src/Entity/Attachment.php)
* [AttachmentThumbnail](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/src/Entity/AttachmentThumbnail.php)

The plugins are able to act by means of the Events system, as elaborated in the
[documentation](https://docs.gnusocial.rocks/developer/storage.html).

