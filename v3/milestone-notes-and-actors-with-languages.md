Milestone: Notes and Actors with languages

Well, it's that, our notes now have a language attribute.

... All right, all right, it's not _just_ it.

## Here's what comes with it:

* [Filter the streams with only the languages you know]()
* [Make Tag Wrangling possible and transversal to languages](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Tag/Tag.php#L135-L146)
* [Federate the language for a more inclusive free network](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/Util/Model/Note.php#L123)

## Here's how it looks

First, the user panel section where the desired preferences are selected:
![User panel language settings section](assets/notes_and_actors_with_languages/settings_language.png)

Upon sending the previous form, the user is redirected to order their selection:
![Ordering the selections made in previous page](assets/notes_and_actors_with_languages/settings_language_order.png)

Finally, when posting the language with the highest priority is selected by default.

However, by accessing "Additional options", another language may be selected. The resulting
note will have the html `lang` attribute according to it.

The posting widget itself:
![Selecting the language of a note when posting](assets/notes_and_actors_with_languages/posting_language_options.png)

## What does this mean?

We can now show you the notes you can read, but for groups, this mean that you
can access umbrella groups and filter the feeds to see what's in your language
and even region.

For too long the fediverse struggled with languages, this step makes it easier
for actual internationalization of the free network.

## A marvellous feed filtered by note language

![](assets/notes_and_actors_with_languages/feed-note-lang-pt.png)

