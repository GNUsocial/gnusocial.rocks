Updates: V3 blog

We initially thought that the milestones list would be enough to
announce the development progress. But it's a little too vague
and doesn't really let us express all the on going changes and efforts.

The [git
history](https://code.undefinedhackers.net/GNUsocial/gnu-social/commits/branch/v3) is clear (we believe), but it can be challenging and obscure to outsiders and non-technical people.

With the introduction of this blog made with
[bashblog](https://github.com/cfenollosa/bashblog), we hope to make all the progress more visible and easier of following :)

It has a [RSS feed](https://www.gnusocial.rocks/v3/feed.rss) so, don't
forget to subscribe!
