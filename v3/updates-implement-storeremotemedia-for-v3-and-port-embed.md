Updates: Implement StoreRemoteMedia for v3 and port Embed

GNU social comes with two plugins that add relevant media functionality.
Not talking neither about ImageEncoder nor VideoEncoder this time, but rather about StoreRemoteMedia and Embed.

[StoreRemoteMedia](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/StoreRemoteMedia)
fetches remote files when they are `Linked` to in a note. I.e., when the major mime type isn't `text`. It triggers thumbnail generation.

[Embed](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/Embed)
attempts to generate a page preview from open graph and oembed. I.e., acts when the major mime type is `text`.

We've changed so much with GNU social v3 that, regarding SRM there was no possible port, just a complete rewrite. Both plugins became smaller and easier to understand while promoting the same functionality with more stability.

