Milestone: ActivityPub

[ActivityPub Plugin source](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub).

This milestone could be just this, what's different from any other ActivityPub
plugin? How is it better than v2's?

It's better in how it's organised and extensible, check the [EVENTS.md](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/EVENTS.md) for examples.

## Video of GNU social v3 exchanging notes with GNU social v2.
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="GNU social v3 federating with GNU social v2 via ActivityPub" src="https://tube.tchncs.de/videos/embed/ca778b22-1af2-4b6f-af3d-f24aac7af9f4" frameborder="0" allowfullscreen></iframe>
