Milestone: Actor colour theme plugin

Actors are now able to set their own colours, through a brand new plugin: "Oomox".
Those accustomed to customising their own desktop should know where the name comes from ;)

## Here's how it works!
The Oomox plugin main class catches the "PopulateProfileSettingsTabs" event upon visiting user panel.

    public function onPopulateProfileSettingsTabs(Request $request, array &$tabs): bool
    {
        $tabs[] = [
            'title'      => 'Light theme colours',
            'desc'       => 'Change the theme colours.',
            'controller' => C\Oomox::oomoxSettingsLight($request),
        ];

        $tabs[] = [
            'title'      => 'Dark theme colours',
            'desc'       => 'Change the theme colours.',
            'controller' => C\Oomox::oomoxSettingsDark($request),
        ];

        return Event::next;
    }

As made evident by the code, two new tabs are added to profile settings, light and dark theme colours.
Since the page styling follows the system theme, actors may want to style each theme differently, therefore they are treated separately.

The actor's defined colours are then saved in the respective entity and cached.
Finally, the colour preferences are used to render the corresponding CSS file which defines the various colour variables used:

    public function oomoxCSS(): Response
    {
        $user = Common::ensureLoggedIn();

        $oomox_table = PluginOomox::getEntity($user);
        if (is_null($oomox_table)) {
            throw new ClientException(_m('No custom colours defined', 404));
        }

        $content = Formatting::twigRenderFile('/oomox/root_override.css.twig', ['oomox' => $oomox_table]);
        return new Response($content, status: 200, headers: ['content-type' => 'text/css']);
    }

Please note, upon rendering for the first time, page render may be blocked until the resulting file is served. Nonetheless, subsequent page renders
won't experience the issue again. That is, if the file is cached by the browser.


## How it looks

Tabs added using the "PopulateProfileSettingsTabs" event:
![User panel Oomox sections](assets/actor_colour_theme_plugin/settings_change_theme_colours.png)

Changing the dark theme colours!
![Dark theme colours selection](assets/actor_colour_theme_plugin/settings_change_theme_colours3.png)

The result of given changes, please note it's no longer a 'dark' theme. 
Given a valid colour, it's the actor's responsibility whether or not the colours make sense. So, go wild!
![The resulting colours in action!](assets/actor_colour_theme_plugin/settings_change_theme_colours4.png)

