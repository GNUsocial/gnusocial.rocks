Updates: Improve the Attachments system

With the milestone [Port Media system from
v2](https://www.gnusocial.rocks/v3/milestone-port-media-handling-from-v2.html) we had ported the existing
attachment system. The fact is that this system wasn't good enough.

It's always important to start with the original code as that allows us
to review past decisions and understand all the corner cases previously
considered.

Sadly, what motivated us to re-design the attachment system were the
non-considered corner cases. In particular:

* Remove `title` from attachment, as it's part of the relation between attachment and note.
* Remove `actor` from attachment, many actors may publish the same attachment.
* Remove `is_local` from attachment,  as it's part of the relation between attachment and note.
* Remove `remote_url` from attachment, different urls can return the same attachment.
* Attachment now has a reference counter 
* Add entity GSActorToAttachment
* Add entity GSActorToRemoteURL
* Add entity RemoteURL
* Add entity RemoteURLToNote
* Add entity RemoteURLToAttachment
* AttachmentToNote now has a title attribute

Key commits:

* [32ad5dbd74 - [ENTITY] Split Attachment in various new entities](https://code.undefinedhackers.net/GNUsocial/gnu-social/commit/32ad5dbd74ebb369c4b3ed688e1fb78464fabe49)
* [c381e58d33 - [ENTITY] Refactor RemoteURL entities to Link](https://code.undefinedhackers.net/GNUsocial/gnu-social/commit/c381e58d3307e7dd07abeb9cf5b3998e34f5bd48)

The structural changes and how it compares to v2 is detailed in our
[wiki](http://agile.gnusocial.rocks/doku.php?id=attachment).

Some relevant observations:

- `Link` is now part of GNU social Data Representation terms. It represents
any remote URL being shared in a note.
- There's only one fixed directory for GSFiles.
