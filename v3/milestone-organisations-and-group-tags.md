Milestone: Organisations and Group Tags

This milestone elaborates on the [Local Groups update](https://www.gnusocial.rocks/v3/updates-local-groups.html). We have decided that internally a organisation would be a specialization of the Group Actor. The same way we handle happenings, polls, articles, and pages as specializations of notes.

It is already possible to create organization actors,
![](assets/groups/create_org.png)

and too have self tags on groups, which then aid on discoverability.
![](assets/groups/set_tags.png)
