Milestone: Web Monetization

[Web Monetization](https://webmonetization.org/) is being proposed as a W3C
standard at the Web Platform Incubator Community Group.

GNU social now supports that initiative with the [Web Monetization plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/WebMonetization).

With this, we also introduce an ActivityPub ([FEP-8c3f](https://codeberg.org/fediverse/fep/pulls/1)) GS extension
`gs:webmonetizationWallet`. This enables actors to support other actors with Web
Monetization in the fediverse.

It looks like this:
## Own profile
![](assets/web_monetization/address.png)
## In the profile of other actors
![](assets/web_monetization/donate.png)

