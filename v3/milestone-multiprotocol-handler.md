Milestone: Multiprotocol Handler

In GNU social version 2 this was accomplished by using a module named `TheFreeNetwork` that would act as a mediator in specific operations concerning the federation plugins.

In version 3 this was simplified around the [`Notification`
component](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/components/Notification/Notification.php#L112-L119),
which asks the [`FreeNetwork` component](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/components/FreeNetwork/FreeNetwork.php#L499-L505) to procedurally call the handlers in the protocol handlers. Although this works via events, we are modelling a service behaviour here.

The remote notification queue is also handled by the `Notification` component.
`TheFreeNetwork` component became devoted to handling WebFinger-related
matters, it's involvement in this is just defining sequence, helpers and setting the API
for the various protocol plugins:

* [`onAddFreeNetworkProtocol`](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/ActivityPub.php#L284): Let FreeNetwork Component know a protocol exists and which class to use to call the `freeNetworkDistribute` method
* [`FreeNetworkActorProtocol::protocolSucceeded`](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/ActivityPub.php#L139): If the protocol plugin can handle a certain actor, register it
* [`onFreeNetworkActorCanAdmin`](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/ActivityPub.php#L207): Fill Actor->canAdmin() for Actors that came from a certain Protocol
* [`freeNetworkGrabRemote`](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/ActivityPub.php#L297): The `FreeNetwork` component will call this function to pull objects by URI
* [`protocol::freeNetworkDistribute`](https://codeberg.org/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub/ActivityPub.php#L381): The `FreeNetwork` component will call this function to distribute this instance's activities

There are also other methods that the `FreeNetwork` component makes available
for plugins, but are meant for WebFinger-related helpers such as `onFreeNetworkFoundXrd` and `FreeNetworkActorProtocol::canIAddr`.
