Milestone: Pinboard API

This builds on top of GNU social bookmarks functionality.

![](assets/bookmarks/pinboard/pinboard-settings.png)

[Most endpoints of Pinboard v1 API were implemented](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/Pinboard/Pinboard.php#L45). We've left out some such as tag delete (as one can't really delete one in GNU social), among others that don't really fit. We have tested our implementation against [Pinkt](https://github.com/fibelatti/pinboard-kotlin) and we will soon be contacting the maintainer so the functionality of using a different `API_TARGET` is included with this app (as it was all we had to change to have this working).

Discussion regarding [how bookmarks are better expressed in terms of ActivityStreams 2.0](https://socialhub.activitypub.rocks/t/explicitly-attached-links/2357/16) is also developing in SocialHub.


