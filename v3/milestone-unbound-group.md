Milestone: Unbound Group

This was quite a long milestone that builds on top of everything we've been
working on. From the [Notification](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/Notification) and [FreeNetwork](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/components/FreeNetwork) components to the [ActivityPub](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/ActivityPub) plugin.

We have started with
[FEP-8485 Unbound Actor](https://codeberg.org/GNUsocial/fep/src/branch/fep-8485-bkp/feps/fep-8485.md), aiming at a ValueFlows based relationship between any kind of actors.

And this was discussed, re-thought, re-designed, and went through various
iterations on [SocialHub Thread Decentralised Group](https://socialhub.activitypub.rocks/t/decentralised-group/2200).

It wasn't until we had finished implementing the federation of Groups,
polishing our Notification system, and defined how we wanted to represent all of
this internally for GNU social v3, that we fresh started with all these ideas in
mind and came with the simpler [FEP-2100 Unbound Group and Organization](https://codeberg.org/GNUsocial/fep/src/branch/fep-8485/feps/fep-2100.md).

Which instead builds on top of the already existing logic for Following. Of
course, this FEP assumes how GNU social (and Lemmy, Friendrica, and Lotide)
understands groups (and organisations). But we believe that, now that
implementations are using these ideas and strategies, after a few more testing
time, everything should become reasonably specified and formalized. Being this
FEP already a step and contribution in that direction.

This FEP was implemented in GS [via a plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/UnboundGroup).

![](assets/unbound/settings_link.png)
![](assets/unbound/example_linked_note.png)
