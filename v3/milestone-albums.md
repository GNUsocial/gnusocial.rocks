Milestone: Albums

This is a natural follow up to the [Collections Milestone](https://www.gnusocial.rocks/v3/milestone-collections.html).

An album is a specialisation of an attachment collection, which is itself a
specialisation of a collection.

Albums were implemented with the [Attachment Collections plugin](https://code.undefinedhackers.net/GNUsocial/gnu-social/src/branch/v3/plugins/AttachmentCollections).

## Here's how it looks
### Adding an attachment (this is in the right sidebar of attachment pages)
![](assets/collection/add_to_collection.png)

### List of collections
![](assets/collection/meta.png)

### Viewing a collection of attachments
![](assets/collection/attachments.png)

