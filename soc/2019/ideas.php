<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Google Summer of Code 2019 Ideas | GNU social</title>
        <link rel="icon" href="../../favicon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://hackersatporto.com/assets/css/main.css">
        <style>
            video {
                width: 100%;
                height: auto;
            }
            body {
                /*max-width: 81em;*/
                max-width: 52em;
                /*background: #fefefe;*/
            }
            
            /* Youtube video old
            .video-container {
                position: relative;
                padding-bottom: 56.25%;
                padding-top: 30px; height: 0; overflow: hidden;
            }

            .video-container iframe,
            .video-container object,
            .video-container embed {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
            
            /* Youtube video
            .videoWrapper {
                 position: relative;
                 width: 100%;
                 height: 0;
                 background-color: #000;
            }
            .videoWrapper169 {
                 padding-top: 56%;
            }
            .videoIframe {
                 position: absolute;
                 top: 0;
                 right: 0;
                 bottom: 0;
                 left: 0;
                 width: 100%;
                 height: 100%;
                 background-color: transparent;
            }
            .videoPoster {
                 position: absolute;
                 top: 0;
                 right: 0;
                 left: 0;
                 width: 100%;
                 height: 100%;
                 margin: 0;
                 padding: 0;
                 cursor: pointer;
                 border: 0;
                 outline: none;
                 background-position: 50% 50%;
                 background-size: 100% 100%;
                 background-size: cover;
                 text-indent: -999em;
                 overflow: hidden;
                 opacity: 1;
                 -webkit-transition: opacity 800ms, height 0s;
                 -moz-transition: opacity 800ms, height 0s;
                 transition: opacity 800ms, height 0s;
                 -webkit-transition-delay: 0s, 0s;
                 -moz-transition-delay: 0s, 0s;
                 transition-delay: 0s, 0s;
            }
            .videoPoster:before {
                 content: '';
                 position: absolute;
                 top: 50%;
                 left: 50%;
                 width: 80px;
                 height: 80px;
                 margin: -40px 0 0 -40px;
                 border: 5px solid #fff;
                 border-radius: 100%;
                 -webkit-transition: border-color 300ms;
                 -moz-transition: border-color 300ms;
                 transition: border-color 300ms;
            }
            .videoPoster:after {
                 content: '';
                 position: absolute;
                 top: 50%;
                 left: 50%;
                 width: 0;
                 height: 0;
                 margin: -20px 0 0 -10px;
                 border-left: 40px solid #fff;
                 border-top: 25px solid transparent;
                 border-bottom: 25px solid transparent;
                 -webkit-transition: border-color 300ms;
                 -moz-transition: border-color 300ms;
                 transition: border-color 300ms;
            }
            .videoPoster:hover:before, .videoPoster:focus:before {
                 border-color: #f00;
            }
            .videoPoster:hover:after, .videoPoster:focus:after {
                 border-left-color: #f00;
            }
            .videoWrapperActive .videoPoster {
                 opacity: 0;
                 height: 0;
                 -webkit-transition-delay: 0s, 800ms;
                 -moz-transition-delay: 0s, 800ms;
                 transition-delay: 0s, 800ms;
            }*/
        </style>
    </head>
    <body>
        <header id="header">
            <nav id="side-menu">
                <label for="show-menu" id="menu-button">Menu</label>
                <input id="show-menu" role="button" type="checkbox">
                <ul id="menu">
                    <li><a href="../"><strong>&larr; GS GSoC</strong></a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#todo">Ideas</a></li>
                    <li><a href="#apply">Apply</a></li>
                </ul>
            </nav>
            <h1>GNU social Summer of Code 2019 - WARNING: THIS IS AN ARCHIVE OF OUR 2019 IDEAS PAGE</h1>
            <strong>For the latest ideas page, <a href="https://www.diogo.site/projects/GNU-social/soc/current/">click here</a>.</strong>
            <p>Organized by <strong><a href="https://www.diogo.site/">Diogo Cordeiro</a></strong></p>
            <p>Mentors: <a href="https://github.com/dansup">Daniel Supernault</a>, <a href="https://www.diogo.site/">Diogo Cordeiro</a> and <a href="https://mmn-o.se/">Mikael Nordfeldth</a></p>
        </header>
        <article id="about">
            <?php
                $video_data = json_decode(file_get_contents("https://you-link.herokuapp.com/?url=https://www.youtube.com/watch?v=z_3dsP_FCAM"));
            ?>
            <h2>Ready?</h2>
            <!-- HTML video -->
            <video width="720" controls class="image" style="margin-left: auto; margin-right: auto; display: block" poster="../../images/youtube-poster-gs-on-gsoc.png">
                <source src="<?php echo $video_data[1]->url; ?>" type="video/mp4">
                <source src="../../videos/gs-on-gsoc.webm" type="video/webm">
            </video>

            <!-- Youtube video
            <div onclick="this.nextElementSibling.style.display='block'; this.style.display='none'">
                <img src="../../images/youtube-poster-gs-on-gsoc.png" style="cursor:pointer" />
            </div>
            <div style="display:none">
                <div class="video-container"><iframe width="853" height="480" src="https://www.youtube-nocookie.com/embed/z_3dsP_FCAM?modestbranding=1&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
            </div>
            <div class="videoWrapper videoWrapper169 js-videoWrapper">
              <!-- YouTube iframe. -->
              <!-- note the iframe src is empty by default, the url is in the data-src="" argument -->
              <!-- also note the arguments on the url, to autoplay video, remove youtube adverts/dodgy links to other videos
              <iframe class="videoIframe js-videoIframe" src="" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube-nocookie.com/embed/z_3dsP_FCAM?autoplay=1&amp; modestbranding=1&amp;rel=0" frameborder="0"></iframe>
              <!-- the poster frame - in the form of a button to make it keyboard accessible
              <button class="videoPoster js-videoPoster" style="background-image:url(../../images/small-poster-gs-on-gsoc.png);">Play video</button>
            </div>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script>
            // poster frame click event
            $(document).on('click', '.js-videoPoster', function (ev) {
              ev.preventDefault();
              var $poster = $(this);
              var $wrapper = $poster.closest('.js-videoWrapper');
              videoPlay($wrapper);
            });

            // play the targeted video (and hide the poster frame)
            function videoPlay($wrapper) {
              var $iframe = $wrapper.find('.js-videoIframe');
              var src = $iframe.data('src');
              // hide poster
              $wrapper.addClass('videoWrapperActive');
              // add iframe src in, starting the video
              $iframe.attr('src', src);
            }
            </script>-->
            <br>
            <p>GNU social is a social communication software used in <a href="https://blog.diogo.site/posts/what-is-the-fediverse">federated social networks</a>. It is widely supported and has a large userbase. It is already used by the Free Software Foundation, and Richard Stallman himself.</p>
            <p>If you would like to know how is it like to be a GSoC student at GNU social, <a href="https://blog.diogo.site/posts/gsoc-2018">read this blog post</a>!</p>
            <h3>What would be expected from me?</h3>
            <dl>
                <dt><strong>Reliability Engineering</strong></dt>
                <dd>Fix bugs of current code base and ensure it stays functional</dd>
                <dt><strong>Software Engineering</strong></dt>
                <dd>Further development of GNU social v2</dd>
                <dt><strong>Computer Security Engineering</strong></dt>
                <dd>Ensure GS is secure</dd>
            </dl>
            <p>Every student will have to pick tasks in each of these fields in GS's GSoC. Excited already? Then read below some of our ideas and learn how you can be part of this!</p>
            <p>Difficulty varies per task but in general GS's SoC is demanding, so you better be honestly interested and willing to work hard and learn a lot.</p>
        </article>
        <article id="todo">
            <h2>Ideas</h2>
            <p>Below is a list of (not very defined) ideas of things/areas you can work on this summer at GNU social. They are just ideas, not full proposals. You should pick some of them that seem to be related and talk on IRC about it. The community will help you understanding what has to be done so you can put a good proposal together.</p>
            
            <h3 id="todo-finish-ap-plugin">Make the ActivityPub plugin started in previous GSoC reliable</h3>
            <p>In previous GSoC an ActivityPub plugin for GS was developed but isn't compatible with every piece of software used in the fediverse (only working with Mastodon so far) nor completely safe and robust for large usage.</p>
            <p>Some of the existing key problems are:</p>
            <ul>
                <li>It is unable to de-duplicate when OStatus and ActivityPub are both enabled - mostly noticed between GS instances with both enabled;</li>
                <li>Is unable to verify signatures which is an attack vector that makes the plugin unsafe for use right now;</li>
                <li>It also doesn't make use of queues nor collections caching nor has any kind of circuit breaker implemented. This makes its use risky in a highly active GS instance.</li>
            </ul>

            <h3 id="todo-storage">Optimize how storage is used</h3>
            <p>Add support for temporary posts: This will allow support for "stories" and empower whole instances (and/or single users) to have temporary posts.</p>
            <p>Optimize OEmbed plugin and further development on image handler: Work on this has already been started.</p>
            
            <h3 id="todo-third-party-developer-interfaces">Review third party developer interfaces</h3>
            <p>There are various bots in the fediverse and we feel it might be about time to set them apart from humans so that we know who to kill when the robotic uprising comes, so to speak (ahem).</p>
            <p>Ultimately, bots aren&rsquo;t humans and, therefore, they shouldn&rsquo;t have accounts claiming that they are &ldquo;Persons&rdquo;.</p>
            <p>So some good measures for making change are:</p>
            <ul>
                <li>Introduce <strong>OAuth 2</strong> in GNU social for authentication of clients that aren&rsquo;t browsers;</li>
                <li>Review current APIs for <strong>Tools</strong> that act in the User&rsquo;s behalf and introduce a user platform for the management of those;</li>
                <li>Review current APIs for <strong>Bots</strong> that act in their own behalf and introduce a developer platform for the management of those.</li>
            </ul>
            <p>Technical sidenote for those who have read the ActivityPub standard: GS Bots are represented as Actors with the <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-application">Application</a> type, while GS Tools are an internal (not federated) thing that allow Actors with the <a href="https://www.w3.org/TR/activitystreams-vocabulary/#dfn-person">Person</a> type (commonly referred to as Users) to use third party tools to control their account.</p>

            <h3 id="todo-gs2">&ldquo;We have you noticed we&rsquo;ve straighten every dent up&rdquo;</h3>
            <p>Props to <a href="https://loadaverage.org/XRevan86">XRevan86</a> for that pun! (Notices in GNU social are colloquially referred to as Dents).</p>
            <p>GNU social wants to get polished with a <span id="todo-themes">modern looking default theme</span>. For this, it would be interesting to come up with some themes guidelines so, if you&rsquo;re into web design, this might be <em>the task</em> for you! :)</p>
            <p>On the other hand, if you are more into backend development, we&rsquo;ve got you covered. Some new and interesting concepts in the world of Software Engineering were introduced and we are naturally looking forward to include them and refresh the current code base for the release of GNU social 2 with them!</p>
        </article>
        <article id="apply">
            <h2>How to apply?</h2>
            <p>First read <a href="https://www.gnu.org/software/soc-projects/guidelines.html">GNU's guidelines</a> on how to prepare a good proposal.</p>
            <p>Then please contact us on <a href="https://agile.gnusocial.rocks/doku.php?id=development_discussion">GS's Development chat</a> to get started on your proposal. For an example proposal, you can refer to <a href="https://www.diogo.site/projects/GNU-social/soc/2018/proposal.pdf">AP plugin proposal</a>.</p>
            <p>We also suggest that you <a href="https://gnusocial.network/try/">create an account in the fediverse</a>.</p>
            <p>You can contact Diogo either on the above mentioned IRC channel (under the nick: up201705417), <a href="https://www.diogo.site/#contact">by email</a> or on his <a href="https://loadaverage.org/diogo">GNU social profile</a>.</p>
            <a href="https://summerofcode.withgoogle.com/" class="BigButton"><strong>GO!</strong></a>
        </article>
    </body>
</html>
 
