<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Google Summer of Code 2020 Ideas | GNU social</title>
        <link rel="icon" href="../../favicon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://hackersatporto.com/assets/css/main.css">
        <style>
            video {
                width: 100%;
                height: auto;
            }
            body {
                /*max-width: 81em;*/
                max-width: 52em;
                /*background: #fefefe;*/
            }

            /* Youtube video old
            .video-container {
                position: relative;
                padding-bottom: 56.25%;
                padding-top: 30px; height: 0; overflow: hidden;
            }

            .video-container iframe,
            .video-container object,
            .video-container embed {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            /* Youtube video
            .videoWrapper {
                 position: relative;
                 width: 100%;
                 height: 0;
                 background-color: #000;
            }
            .videoWrapper169 {
                 padding-top: 56%;
            }
            .videoIframe {
                 position: absolute;
                 top: 0;
                 right: 0;
                 bottom: 0;
                 left: 0;
                 width: 100%;
                 height: 100%;
                 background-color: transparent;
            }
            .videoPoster {
                 position: absolute;
                 top: 0;
                 right: 0;
                 left: 0;
                 width: 100%;
                 height: 100%;
                 margin: 0;
                 padding: 0;
                 cursor: pointer;
                 border: 0;
                 outline: none;
                 background-position: 50% 50%;
                 background-size: 100% 100%;
                 background-size: cover;
                 text-indent: -999em;
                 overflow: hidden;
                 opacity: 1;
                 -webkit-transition: opacity 800ms, height 0s;
                 -moz-transition: opacity 800ms, height 0s;
                 transition: opacity 800ms, height 0s;
                 -webkit-transition-delay: 0s, 0s;
                 -moz-transition-delay: 0s, 0s;
                 transition-delay: 0s, 0s;
            }
            .videoPoster:before {
                 content: '';
                 position: absolute;
                 top: 50%;
                 left: 50%;
                 width: 80px;
                 height: 80px;
                 margin: -40px 0 0 -40px;
                 border: 5px solid #fff;
                 border-radius: 100%;
                 -webkit-transition: border-color 300ms;
                 -moz-transition: border-color 300ms;
                 transition: border-color 300ms;
            }
            .videoPoster:after {
                 content: '';
                 position: absolute;
                 top: 50%;
                 left: 50%;
                 width: 0;
                 height: 0;
                 margin: -20px 0 0 -10px;
                 border-left: 40px solid #fff;
                 border-top: 25px solid transparent;
                 border-bottom: 25px solid transparent;
                 -webkit-transition: border-color 300ms;
                 -moz-transition: border-color 300ms;
                 transition: border-color 300ms;
            }
            .videoPoster:hover:before, .videoPoster:focus:before {
                 border-color: #f00;
            }
            .videoPoster:hover:after, .videoPoster:focus:after {
                 border-left-color: #f00;
            }
            .videoWrapperActive .videoPoster {
                 opacity: 0;
                 height: 0;
                 -webkit-transition-delay: 0s, 800ms;
                 -moz-transition-delay: 0s, 800ms;
                 transition-delay: 0s, 800ms;
            }*/
        </style>
    </head>
    <body>
        <header id="header">
            <nav id="side-menu">
                <label for="show-menu" id="menu-button">Menu</label>
                <input id="show-menu" role="button" type="checkbox">
                <ul id="menu">
                    <li><a href="../"><strong>&larr; GS GSoC</strong></a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#ideas">Ideas</a></li>
                    <li><a href="#apply">Apply</a></li>
                </ul>
            </nav>
            <h1>GNU social Summer of Code 2020 - WARNING: THIS IS AN ARCHIVE OF OUR 2020 IDEAS PAGE</h1>
            <strong>For the latest ideas page, <a href="/soc/">click here</a>.</strong>
            <p>Organized by <strong><a href="https://www.diogo.site/">Diogo Cordeiro</a></strong></p>
            <p>Mentors: <a href="https://www.diogo.site/">Diogo Cordeiro</a>, <a href="https://loadaverage.org/XRevan86">Alexei Sorokin</a>, <a href="https://dansup.com">Daniel Supernault</a> and <a href="http://status.hackerposse.com/rozzin">Joshua Judson Rosen</a></p>
        </header>
        <article id="about">
            <?php
                $video_data = json_decode(file_get_contents("https://you-link.herokuapp.com/?url=https://www.youtube.com/watch?v=z_3dsP_FCAM"));
            ?>
            <h2>Ready?</h2>
            <!-- HTML video -->
            <video width="720" controls class="image" style="margin-left: auto; margin-right: auto; display: block" poster="../../images/youtube-poster-gs-on-gsoc.png">
                <source src="<?php echo $video_data[1]->url; ?>" type="video/mp4">
                <source src="../../videos/gs-on-gsoc.webm" type="video/webm">
            </video>
            <br>
            <p>GNU social is a social communication software used in <a href="https://blog.diogo.site/posts/what-is-the-fediverse">federated social networks</a>. It is widely supported and has a large userbase. It is already used by the Free Software Foundation, and Richard Stallman himself.</p>
            <p>If you would like to know how is it like to be a GSoC student at GNU social, <a href="https://blog.diogo.site/posts/gsoc-2018">read this blog post</a>!</p>
        </article>
        <article id="ideas">
            <h2>Ideas</h2>
            <p>Below is a list of ideas you can work on this Summer at GNU social. You should pick one (or mix a couple) of them and talk on IRC about it. The community will help you understanding what has to be done so you can put a good proposal together.</p>
<br>
            <h3 id="todo-rewrite">Rewrite GNU social using Symfony</h3>
            <p><strong>Description: </strong>The title is pretty much self explanatory. One might either focus in core, plugins or both (this is to say that we may accept two students for this one).</p>
            <p><strong>Mentors: </strong><a href="https://www.diogo.site">Diogo Cordeiro</a>, <a href="https://loadaverage.org/XRevan86">Alexey Sorokin</a></p>
            <p><strong>Proposal: </strong>Make a parallelism between the GS system and Symfony (autoloading, routing, templates, cache, queues, cron, orm/db, ...), indicating how to implement something like our plugin system in such framework.</p>
            <p><strong>Proof of Competence: </strong>Migrate the avatar system as was done with the media system in the past gsoc and a couple more of backlog tasks. Additional big crunch and bang with a minimally working plugin system would be ideal.</p>
            <p><strong>Requirements: </strong>PHP7, SQL, Redis, OOP, Declarative and Functional programming. Symfony and Doctrine experience is preferred.</p>
            <p><strong>Difficulty: </strong>Hard</p>
            <p><strong>Study Resources: </strong>Refer to the generic ones in "Reliability Engineering". Study Symfony's docs as well as Redis's.</p>
<br>
            <h3 id="todo-federation">Federation</h3>
            <p><strong>Description: </strong>To implement Federation in v3.</p>
            <p><strong>Mentors: </strong><a href="https://dansup.com">Daniel Supernault</a>, <a href="https://www.diogo.site">Diogo Cordeiro</a> (as secondary)</p>
            <p><strong>Proposal: </strong>Demonstrate knowledge of {TFNM, ActivityPub, OStatus, Pleroma API}.</p>
            <p><strong>Proof of Competence: </strong>Finish the TFNM and ensure AP works on v2, write auto tests for these.</p>
            <p><strong>Requirements: </strong>PHP7, Declarative and Functional programming, HTTP, REST APIs. Guzzle experience is preferred.</p>
            <p><strong>Difficulty: </strong>Medium</p>
            <p><strong>Study Resources: </strong>In addition to the generic ones in "Suggest a proposal!": <a href="https://www.diogo.site/projects/ActivityPub_seminar/">ActivityPub</a> and <a href="https://docs-develop.pleroma.social/backend/API/pleroma_api/">Pleroma API</a></p>
<br>
            <h3 id="todo-api">APIs</h3>
            <p><strong>Description: </strong>To implement Pleroma API and port Twitter API from v2 to v3.</p>
            <p><strong>Mentors: </strong><a href="https://www.diogo.site">Diogo Cordeiro</a>, <a href="https://loadaverage.org/XRevan86">Alexey Sorokin</a> (as secondary)</p>
            <p><strong>Proposal: </strong>Demonstrate knowledge of {Twitter API, Pleroma API, OAuth2, Open ID}.</p>
            <p><strong>Proof of Competence: </strong>Ensure GNU social's Twitter API.</p>
            <p><strong>Requirements: </strong>PHP7, Declarative and Functional programming, HTTP, REST APIs. Guzzle experience is preferred.</p>
            <p><strong>Difficulty: </strong>Medium</p>
            <p><strong>Study Resources: </strong>In addition to the generic ones in "Suggest a proposal!":</p>
            <ul>
        <li><a href="httpd://www.indieweb.org/">IndieWeb</a></li>
                <li><a href="https://www.restapitutorial.com/">REST API tutorial</a></li>
                <li>OAuth2:
                    <ul>
                        <li><a href="https://bshaffer.github.io/oauth2-server-php-docs/#learning-the-oauth-standard">BShaffer's lib has a good doc</a></li>
                        <li><a href="https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2">Introduction by Digital Ocean</a></li>
                        <li><a href="https://aaronparecki.com/oauth-2-simplified/">Aaron Parecki's guide</a></li>
                    </ul>
                </li>
            </ul>
<br>
            <h3 id="todo-fronted">New Frontend Classic</h3>
            <p><strong>Description: </strong>To implement the new interface in v3 compatible with the <a href="https://anybrowser.org/campaign/">AnyBrowser campaign</a> and <a href="https://www.gnu.org/software/librejs/">LibreJS</a>.</p>
            <p><strong>Get some inspiration from: </strong>
            <ul>
                <li><a href="https://gnusocial.no/main/public">GNUsocial.no</a></li>
                <li><a href="https://loadaverage.org/main/public">LoadAverage</a></li>
                <li><a href="https://matriu.cat/main/all">matriu.cat</a></li>
                <li><a href="https://gnusocial.cc/main/all">gnusocial.cc</a></li>
                <li><a href="https://social.hackersatporto.com">Hackers at Porto</a></li>
            </ul>
            <p><strong>NB: </strong>While designing, bear in mind three things: <a href="https://emsenn.net/essays/cultural-guide-for-fediverse-newcomers/">fediverse culture</a>, JS is optional, it should be just an improvement of UX when it's supported by the browser (as elaborated in AnyBrowser), accessibility is a must - bear in mind screen readers, colour blindness, etc.</p>
            <p><strong>Mentors: </strong><a href="http://status.hackerposse.com/rozzin">Joshua Judson Rosen</a>, <a href="https://dansup.com">Daniel Supernault</a> (secondary)</p>
            <p><strong>Proposal: </strong><a href="https://en.wikipedia.org/wiki/Website_wireframe">Wireframe</a> and <a href="https://en.wikipedia.org/wiki/Mockup">Mockup</a> of the interface. You will definitely want to request community feedback while working on this.</p>
            <p><strong>Proof of Competence: </strong>Make a new website in Hugo or similar for <a href="https://gnusocial.network">https://gnusocial.network</a> (which is GNU social's landing page).</p>
            <p><strong>Requirements: </strong>CSS3 and HTML5. JS and Twig experience is preferred.</p>
            <p><strong>Difficulty: </strong>Easy</p>
            <p><strong>Study Resources: </strong>Refer to the generic ones in "Suggest a proposal!".</p>
<br>
            <h3 id="todo-fronted-pleroma">New Frontend Modern</h3>
            <p><strong>Description: </strong>To implement a GNU social UI for Pleroma API compatible with <a href="https://www.gnu.org/software/librejs/">LibreJS</a>. This will be a plugin in v3.</p>
            <p><strong>Get some inspiration from: </strong>
            <ul>
                <li><a href="https://gnusocial.no/main/public">GNUsocial.no</a></li>
                <li><a href="https://loadaverage.org/main/public">LoadAverage</a></li>
                <li><a href="https://matriu.cat/main/all">matriu.cat</a></li>
                <li><a href="https://gnusocial.cc/main/all">gnusocial.cc</a></li>
                <li><a href="https://social.hackersatporto.com">Hackers at Porto</a></li>
            </ul>
            <p><strong>NB: </strong>While designing, bear in mind the <a href="https://emsenn.net/essays/cultural-guide-for-fediverse-newcomers/">fediverse culture</a>.</p>
            <p><strong>Mentors: </strong><a href="https://dansup.com">Daniel Supernault</a>, <a href="http://status.hackerposse.com/rozzin">Joshua Judson Rosen</a> (secondary)</p>
            <p><strong>Proposal: </strong><a href="https://en.wikipedia.org/wiki/Website_wireframe">Wireframe</a> and <a href="https://en.wikipedia.org/wiki/Mockup">Mockup</a> of the interface. You will definitely want to request community feedback while working on this.</p>
            <p><strong>Proof of Competence: </strong>Make a new website in Hugo or similar for <a href="https://gnusocial.network">https://gnusocial.network</a> (which is GNU social's landing page). Please follow the same guidelines of the Classic idea for this.</p>
            <p><strong>Requirements: </strong>CSS3, HTML5, JS, Bootstrap and <a href="https://docs-develop.pleroma.social/backend/API/pleroma_api/">Pleroma API</a>. Twig, Vue and Webpack experience is preferred.</p>
            <p><strong>Difficulty: </strong>Easy</p>
            <p><strong>Study Resources: </strong>Refer to the generic ones in "Suggest a proposal!".</p>
<br>
            <h3 id="todo-reliability">Reliability Engineering</h3>
            <p><strong>Description: </strong>To test v3.</p>
            <p><strong>Mentors: </strong><a href="https://loadaverage.org/XRevan86">Alexey Sorokin</a>, <a href="http://status.hackerposse.com/rozzin">Joshua Judson Rosen</a> (as secondary)</p>
            <p><strong>Proposal: </strong>Monography on DevOps applied to GNU social v3 Summer (5000 words)</p>
            <p><strong>Proof of Competence: </strong>Write/review tests of v2, test its security. Develop a platform for daily reports of GNU social SoC students throughout Summer.</p>
            <p><strong>Requirements: </strong>PHP7, REST APIs, Shell Script. PHPUnit experience is preferred.</p>
            <p><strong>Difficulty: </strong>Easy</p>
            <p><strong>Study Resources: </strong>In addition to the generic ones in "Suggest a proposal!":</p>
            <ul>
                <li><a href="https://www.cl.cam.ac.uk/~rja14/book.html">EBook on Cybersecurity</a></li>
                <li><a href="https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-170-software-studio-spring-2013/">APIs, Modelling, etc.</a></li>
                <li><a href="https://ocw.mit.edu/courses/aeronautics-and-astronautics/16-355j-software-engineering-concepts-fall-2005/">Dev Cycles</a></li>
                <li><a href="https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-005-software-construction-spring-2016/">Testing and Code Reviews</a></li>
                <li><a href="https://en.m.wikipedia.org/wiki/DevOps">Wikipedia on DevOps</a></li>
                <li><a href="https://en.wikipedia.org/wiki/Code_coverage">Wikipedia on Code Coverage</a></li>
                <li><a href="https://en.wikipedia.org/wiki/Quality_assurance">Wikipedia on Quality Assurance</a></li>
                <li><a href="https://en.wikipedia.org/wiki/Quality_control">Wikipedia on Quality Control</a></li>
            </ul>
<br>
            <h3 id="todo-suggest">Suggest a proposal!</h3>
            <p><strong>Proposal: </strong>See issues labeled as "enhancement" in our repo and propose one or more plugins :)</p>
            <p><strong>Proof of Competence: </strong>Backlog tasks or other more appropriate.</p>
            <p>Also, be sure to checkout our <a href="https://notabug.org/diogo/gnu-social/src/nightly/TODO.md">TODO</a> if you want to start preparing yourself (or, better yet, contributing early)!</p>
            <p><strong>Study Resources: </strong></p>
            <ul>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/http/">Briefing on HTTP</a></li>
                <li><a href="https://www.restapitutorial.com/httpstatuscodes.html">HTTP Status codes for semantic REST APIs</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/php/">PHP 5.6 (far from perfect but it's a gentle and brief introduction)</a></li>
                <li><a href="https://docstore.mik.ua/orelly/webprog/php/index.htm">Programming PHP (for a more comprehensive and slow introduction)</a></li>
                <li><a href="https://docstore.mik.ua/orelly/webprog/pcook/index.htm">PHP Cookbook (for advanced topics in PHP)</a></li>
                <li><a href="https://www.tutorialspoint.com/php7/index.htm">The new stuff of PHP 7 </a></li>
                <li><a href="https://medium.com/@rtheunissen/efficient-data-structures-for-php-7-9dda7af674cd">Data Structures in PHP</a></li>
                <li><a href="http://phptherightway.com">PHP guidelines</a></li>
                <li><a href="https://docs.phpdoc.org/latest/guides/index.html">PHP DocBlock guidelines</a></li>
                <li><a href="https://www.php-fig.org/psr/">PHP PSR guidelines</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/security/">Huge TL;DR on WebSecurity</a></li>
                <li><a href="https://owasp.org/www-project-web-security-testing-guide/assets/archive/OWASP_Testing_Guide_v4.pdf">OWASP's Web Testing Guide</a></li>
                <li><a href="https://www.gkogan.co/blog/simple-systems/">Simple Systems Have Less Downtime</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/xml/">Briefing on XML</a></li>
                <li><a href="https://cs.stanford.edu/people/widom/DB-mooc.html">Databases</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/patterns/">Briefing on Design Patterns</a></li>
                <li><a href="https://en.wikipedia.org/wiki/Object-relational_mapping">ORM</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/solid/">Briefing on SOLID</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/html5/">Briefing on HTML5</a></li>
                <li><a href="https://web.fe.up.pt/~arestivo/presentation/css3/">Briefing on CSS3</a></li>
            </ul>
        </article>
        <article id="apply">
            <h2>How to apply?</h2>
            <p><strong>NB: The "Proof of Competence" has to be done together with the proposal in your application!</strong></p>
            <p>First read <a href="https://www.gnu.org/software/soc-projects/guidelines.html">GNU's guidelines</a> on how to prepare a good proposal.</p>
            <p><strong>Suggestion:</strong>
            <ul>
                <li>Header:</li>
                <ul>
                    <li>Name</li>
                    <li>Email</li>
                    <li>Other contact forms (IRC, XMPP)</li>
                    <li>Timezone</li>
                    <li>Project name</li>
                    <li>*Proof of Competence link</li>
                </ul>
                <li>Summary</li>
                <li>Benefits</li>
                <li>Deliverables</li>
                <li>*State Of The Art</li>
                <li>*Relevant Research</li>
                <li>Plan</li>
                <li>Tentative Timeline</li>
                <li>Communication</li>
                <li>Qualification</li>
                <li>*References</li>
            </ul>
            <pre>
            * - if applicable
            N.B.:
            - Plan and Timeline may be together
            - Deliverables may come up after timeline
            - You're allowed to create subsections and even sections
            </pre>
            </p>
            <p>Then please contact us on <a href="https://agile.gnusocial.rocks/doku.php?id=development_discussion">GS's Development chat</a> to get started on your proposal. For an example proposal, you can refer to <a href="/soc/2019/accepted_proposals/network.pdf">Network Services Improvements proposal from last year</a>.</p>
            <p>In this year proposal, you should also specify your timezone. With respect to the time you will have to dedicate, GSoC demands 30h to 40h of work per week. GNU social's Summer of Code expects you to work an average of 36.5h/week, you can organize that time as you please, but you must be sure to dedicate that in your weekly work or to be overly productive.</p>
            <p>We suggest you to do a four-day work week with 6h of work/day + 3h to document, review/test and report the progress you've done (you usually won't need that much for this and we won't complain as long as you're doing well/being highly productive). As breaks are important, we recommend a 1h lunch break, 15min break after 4h of continuous work and a further 15mins break after 6h of work. These breaks won't be considered as part of your work time.</p>
            <p>Note that 6h*4 = 24h, which is below the minimum 30h demanded by Google, if you only do the 24h/week, you'll have to prove your worth. Otherwise, we might require that you either do a 5-day week or that you scale it up to 7.5h in your 4-day week.</p>
            <p>In some places, GSoC starts in the middle of college's last exam season, if that's your case, we accept that you do less hours of work in the beginning as long as you compensate those later.</p>
            <p>In general, you will always have to work at least 120h/month, ideally 146h/month (or the productively equivalent). We do not accept that you transfer expected work time from a month to another. Nonetheless, an under-performing week will make us request more hours from you in the week after (up to the limit of 40h).</p>
            <p>We also suggest that you <a href="/communities/">create an account in the fediverse</a>.</p>
            <p>You can contact Diogo either on the above mentioned IRC channel (under the nick: includeals), <a href="https://www.diogo.site/#contact">by email</a> or on his <a href="https://loadaverage.org/diogo">GNU social profile</a>.</p>
            <p>If you need a ZNC Bouncer, you can ask enot for one in IRC. Or just use our XMPP bridge, refer to our project's README to learn more.</p>
            <a href="https://summerofcode.withgoogle.com/" class="BigButton"><strong>GO!</strong></a>
        </article>
    </body>
</html>
