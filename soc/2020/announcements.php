<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>2020 | GNU social Summer of Code</title>
        <link rel="icon" href="../../favicon.png">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://hackersatporto.com/assets/css/main.css">
    </head>
    <body>
        <header id="header">
            <nav id="side-menu">
                <label for="show-menu" id="menu-button">Menu</label>
                <input id="show-menu" role="button" type="checkbox">
                <ul id="menu">
                    <li><a href="../"><strong>&larr; GS GSoC</strong></a></li>
                    <li><a href="#" class="current">Announcements</a></li>
                    <li><a href="academics.html">Assessment</a></li>
                    <li><a href="https://notabug.org/diogo/gnu-social/src/nightly/DOCUMENTATION/DEVELOPERS">Contributing</a></li>
                    <li><a href="daily_report/">Daily Reports</a></li>
                    <li><a href="ideas.php">Proposed Ideas</a></li>
                    <li><a href="../../study_resources.html">Study Resources</a></li>
                </ul>
            </nav>
            <h1>GNU social Summer of Code 2020 - WARNING: THIS IS AN ARCHIVE OF OUR 2020 PROGRAMME PAGE</h1>
            <strong>For the latest programme page, <a href="https://www.diogo.site/projects/GNU-social/soc/current/">click here</a>.</strong>

            <p>Organized by <strong><a href="https://www.diogo.site/">Diogo Cordeiro</a></strong></p>
            <p>Mentors: <a href="https://www.diogo.site/">Diogo Cordeiro</a>, <a href="https://loadaverage.org/XRevan86">Alexei Sorokin</a>, <a href="https://dansup.com">Daniel Supernault</a>, <a href="https://www.hackerposse.com/~rozzin/">Joshua Judson Rosen</a> and <a href="https://github.com/phablulo">Phablulo Joel</a></p>
        </header>
        <article id="pinned" style="border-style:solid;border-color:coral;padding:1em">
            <h2>Day-to-day links</h2>
            <p>
                <b>Tasks</b><br>
                <a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=show&project_id=1">Kanban board GS v2</a>      [<a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=readonly&token=03795efb8138c4e7661a900c234c0df1bc3fc03cdfcda8619cd5d0e666de">Public</a>]<br>
                <a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=show&project_id=2">Kanban board GS v3</a>      [<a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=readonly&token=d2293e55cabae7cceff9fb496c651328195357d392b9e61a9f229ed6d463">Public</a>]<br>
                <a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=show&project_id=3">Kanban board Pleroma FE</a> [<a href="https://kanban.undefinedhackers.net/?controller=BoardViewController&action=readonly&token=38a6276b337d873d349357796c265403d4ff4bc3d8d11819e849271044b0">Public</a>]<br>
                <a href="daily_report/archive/roadmap.txt">GSoC Roadmap</a><br>
                <a href="https://notabug.org/diogo/gnu-social/issues">Issue tracker</a>
            </p>
            <p>
                <b>Contributing</b><br>
                <a href="https://notabug.org/diogo/gnu-social/src/nightly/DOCUMENTATION/DEVELOPERS/CONTRIBUTING/coding_standards.md">Style guide</a><br>
                <a href="/soc/2020/daily_report/">Daily Reports</a><br>
                <a href="/soc/study_resources.html">Study Resources</a>
            </p>
        </article>
        <article id="announcements">
            <h2>Announcements</h2>
            <h3>Last August Week (--08-24)</h3>
            <p><strong>Final Report Instructions</strong><br>
            The report must have A4 paper geometry with 12pt font-size.
            The pages, sections and figures must be enumerated. Maximum of 5000 words.<!--It's expected a minimum of 5000 words and a maximum of 6500.-->
            These limits don't include cover, indexes nor appendices.
            The report must be well structured.<br><br>
            <b>One example of a suitable format is:</b><br>
            <pre>
- Cover
    Title / Author / Date
- Mentors Page
    Page for the mentors to sign and make the necessary comments (namely a declaration confirming
    the student has evidenced certain knowledge throughout the summer and worked the
    declared hours).  Note that this is mandatory.
- Abstract
    A brief summary of approximately 250 words. Note that this is mandatory.
- Preface
    Explaining when, where, and why the project was carried out, and thanking those who helped.
- Table of contents
- Introduction
    Introduction to the done work, and description of the objectives during the summer.
    Short background, issue and aim, as well as the structure of the report.
- Methodology
    Theories, tools, etc., that were applied in the project.
    Description of used technologies as well as discarded alternatives.
- Results<!--    Description of the done work chronologically sorted and divided in sub-sections.    You must describe the various tasks during the work period, including methods and results.-->
- Discusssion and conclusion
    Reflection on whether the aim was fulfilled, and on the opportunities
    for further development. Critical review and discussion of the results.
- References
    In order to enable the reader to review the project and go to the original sources
    on which the material is based, the report needs a good reference list.
    Continuously filling in the reference list can save you a lot of time
    compared to doing everything at the end.
- Appendices
    Extensive data material that is relevant to the work, but too large
    to incorporate into the text, can be included in an appendix.
            </pre>
            <p><a href="https://libguides.lub.lu.se/c.php?g=526053" target="_top" class="external-link">Information on referencing (Lund University Libraries' website)</a></p>
            <p><a href="http://www.lth.se/fileadmin/lth/anstallda/kvalitet/examensarbete/Guide_foer_populaervetenskapligt_skrivande_141015_-_eng.pdf" target="_top" class="external-link">Guide and checklist for writing a summary of a degree project aiming at a popular science readership (Faculty of Engineering (LTH), Lund University)</a></p>
            <b>The graphic design</b>
            <p>It is better to concentrate more on the content of the report than on the design. Customise the layout to fit the content rather than the other way around.</p>
            <h3>Second Evaluations open July 27 - 31 17:00 (GMT+1)</h3>
            <p>Quick reminder that the GNU social Summer of Code 2020 assessing week has started! You should have opened your Merge Requests by now. Additionally, if you're on GSoC, you need to fill Google's forms.</p>
            <p>Best wishes!</p>
            <h3>First Evaluations open June 30 - July 2 17:00 (GMT+1)</h3>
            <p>Quick reminder that the GNU social Summer of Code 2020 assessing week has started! You should have opened your Merge Requests by now. Additionally, we need you to fill two forms (check your email inbox!).</p>
            <p>These two forms together will only take 10 minutes to complete and are <b>REQUIRED</b> - don't miss the deadline and fail GS SoC because you didn't take 10 minutes to complete the evaluation.</p>
            <p><strong>Deadline for students to complete the evaluation form is Thursday, July 2nd at 17:00 (GMT+1).</strong></p>
            <p>Best wishes!</p>
            <h3>End of June's third week (--06-22)</h3>
            <p><strong>Important Note</strong>:
Congrats to you all on maintaining a sensible amount of hours to work with in this final June's week, this was specially important in this exams season of yours.<br>
This system based on debitable hours per week was a special case due to distributing work with May in order to make it up for exams.
This is <em>not</em> how it will work in the next two months. As was stated in the <a href="https://www.diogo.site/projects/GNU-social/soc/2020/daily_report/">work schedule explanation</a>, there's
a closed interval of hours that we expect you to work per week. Hence, the freedom you had this month regarding transferring work
from a week to another will soon be significantly reduced.<br>
That being said, it's also now opportune to kindly remind you of our <a href="https://www.diogo.site/projects/GNU-social/soc/2020/academics.html">assessing framework</a>,
please note that hours is not even part of what we use to assess, i.e., working more or less hours isn't really considered,
that's just a formal requirement, we care about results. The parameters you will be graded on are:
- <em>Autonomy</em> (Have you bothered the mentors beyong what's healthy?);
- <em>Objectives satisfaction</em> (Have you done all the tasks you were assigned in the roadmap?);
- <em>Difficulty</em> (Were these tasks hard?).<br>
By the end of this week, GSoC assessment will start. Best of work nailing it! :)
            </p>
            <p>
<pre><b>Current status of debitable hours</b>
<u>Hugo</u>
Dedicated hours this week: 12
Expected hours this week: 36.5
Debitable hours next week: 49.5-(36.5-12)= 25

<u>Susanna</u>
Dedicated hours this week: 57.75
Expected hours this week: 36.5
Debitable hours next week: -5.14-(36.5-57.75)= 16.11

<u>Eliseu</u>
Dedicated hours this week: 24
Expected hours this week: 32
Debitable hours next week: 16-(32-24)= 8</pre>
            </p>
            <h3>End of June's second week (--06-15)</h3>
            <p>
<pre><b>Current status of debitable hours</b>
<u>Hugo</u>
Dedicated hours this week: 6
Expected hours this week: 36.5
Debitable hours next week: 80-(36.5-6)= 49.5

<u>Susanna</u>
Dedicated hours this week: 2.15
Expected hours this week: 36.5
Debitable hours next week: 29.21-(36.5-2.15)= -5.14

<u>Eliseu</u>
Dedicated hours this week: 34
Expected hours this week: 32
Debitable hours next week: 14-(32-34)= 16</pre>
            </p>
            <h3>End of June's first week (--06-08)</h3>
            <p>
<pre><b>Current status of May debitable hours</b>
<u>Hugo</u>
Dedicated hours this week: 20
Expected hours this week: 24
24 - 20 = 4h debited from May
84 - 4 = 80 debitable May hours remaining

<u>Susanna</u>
Dedicated hours this week: 29.41
Expected hours this week: 36.5
36.5 - 29.41 = 7.09h debited from May
36.3 - 7.09 = 29.21 debitable May hours remaining

<u>Eliseu</u>
Dedicated hours this week: 23
Expected hours this week: 32
32 - 23 = 9h debited from May
23 - 9 = 14 debitable May hours remaining</pre>
            </p>
            <p>Keep up with the <a href="daily_report/archive/roadmap-as-of-june.txt">roadmap</a> folks!</p>
            <h3>Bonding period end (--06-01)</h3>
            <p>
<pre><b>Total of May dedicated hours per student</b>

Hugo: 84h
Susanna: 36.3h
Eliseu: 23h

<b>State of the tasks</b>

<u>Hugo</u>
The following tasks:
- Port configuration
- Basic interfaces for queuing, caching, etc.
- Basic test structure
have moved to June.

<u>Susanna</u>
May tasks moved all to June.

<u>Eliseu</u>
May tasks moved all to June.</pre>
            </p>
            <h3>Bonding Period Start (--05-04)</h3>
            <p><a href="daily_report/archive/roadmap-as-of-may.txt">GSoC Roadmap published</a> and added to day-to-day links!</p>
            <h3>New landing page (--04-14)</h3>
            <p>Poll results:
                <ul>
                    <li><strong>susanna</strong>: 5</li>
                    <li><strong>pranjal</strong>: 3</li>
                    <li><strong>spookie</strong>: 3</li>
                    <li><strong>suraj</strong>: 0</li>
                </ul>
                Congrats everyone! :)
            </p>
            <h3>New landing page contest (--04-12)</h3>
            <p>The FE prospective students had to write a new landing page as part of their Proof of Competence, we're now <a href="https://www.diogo.site/projects/GNU-social/soc/2020/landing/">running a poll</a> in our IRC channel to decide which one will replace the existing one at <a href="https://gnusocial.network">gnusocial.network</a>.</p>
            <h3>End of GSoC Student Application Period (--03-31)</h3>
            <p>Students who haven't finished their proof of competence are allowed to further work until --04-10. After that we won't consider any further work in the assessment process. Results will be announced in --05-04.</p>
        </article>
    </body>
</html>
