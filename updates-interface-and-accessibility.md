Updates: Interface and accessibility

Hello everyone! Throughout the past year there has been a lot of work in creating a polished, modern looking UI. 
This update has been in the works for a long time... Many considerations had to be done, and given the current state of 
modern browsers (and their over-reliance in JS) many more surfaced, leading to further testing and fixes. 

We hope you like it!

### The prime directive
> Modern looking, consistent and accessible UI across all browsers. 
> Non-JS version as the primary focus, **JS is optional** and should be regarded as such.

The Web is 95% typography, the art and technique of arranging type to make text more readable and pleasing.
To achieve this, a textual hierarchy is fundamental, text should present a clear, readable structure to the reader. 
In much of the same fashion, the way we perceive Web pages relies upon the same fundamentals. As such, by focusing on the
markup, we hope to achieve an accessible, fast and polished structure by which any browser and screen reader relies upon.

### Features
* Accessible
  * Easy to use keyboard only navigation
    * Feedback on focused elements
    * Keyboard shortcuts to access main regions
    * Vi-like shortcuts
  * Screen reader tested
    * Notifies the user when focusing on key elements, such as the feed, notes and actions
    * Brief description of navigation links
    * Using semantic HTML whenever possible
  * Special care given for styling solutions that could break accessibility
  * Colors and type sizes in accordance to W3C contrast guidelines
    * Chosen specifically blue-ish and grey-ish hues for faster page loading perception
* Fast, the content provided to the user really is just markup and some CSS rules
  * Optionally there will be some small use cases where JS makes sense (it's the only thing modern browsers understand e.e)
    * For example, cropping your avatar before uploading a new one.
    * This is the only example until now.
* Dark and light default themes available (according to your system theme)
* Graceful degradation for unsupported HTML elements
* Achieving modern user interface patterns without JS
* Various layout engines tested and work as they should
  * Qt WebEngine (Qutebrowser, Falkon, Otter Browser, etc...)
  * WebkitGTK (Epiphany, Midori, etc...)
  * Gecko (Firefox and derivatives)
  * Goanna (Palemoon, Basilisk, etc...)
  * Internet Explorer

### Video
#### Keyboard tests
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="GNU social v3 Accessibility: Orca reading the Public feed" src="https://tube.tchncs.de/videos/embed/9a16e84c-4150-4849-ac63-019c9a3782d9" frameborder="0" allowfullscreen></iframe>

<hr>

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="GNU social v3 Accessibility: Login, Update nickname" src="https://tube.tchncs.de/videos/embed/8c86754a-8c71-4a28-8e47-8ef83a9b5b35" frameborder="0" allowfullscreen></iframe>

<hr>

### Images
*Default dark theme*
<img src="https://agile.gnusocial.rocks/lib/exe/fetch.php?media=interface:timeline_dark.png" alt="" style="display: block; width: 600px; max-width: 100%;">
<img src="https://agile.gnusocial.rocks/lib/exe/fetch.php?media=interface:settings_dark.png" alt="" style="display: block; width: 600px; max-width: 100%; margin-top: 1rem">
> Settings are re-organized, allowing to focus into view each "tab" using the *details* HTML element.

*Default light theme*
<img src="https://agile.gnusocial.rocks/lib/exe/fetch.php?media=interface:timeline_light.png" alt="" style="display: block; width: 600px; max-width: 100%;">
<img src="https://agile.gnusocial.rocks/lib/exe/fetch.php?media=interface:settings_light.png" alt="" style="display: block; width: 600px; max-width: 100%; margin-top: 1rem">

#### Bonus :')
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="GNU social v3 Accessibility: Eliseu was recording a reply with orca, but I had non-tested changes" src="https://tube.tchncs.de/videos/embed/17a0c9c5-0f1c-4bae-a77d-08b4be67c986?start=54s" frameborder="0" allowfullscreen></iframe>
